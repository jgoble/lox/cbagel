#ifndef bagel_compiler_h
#define bagel_compiler_h

void error(const char* message);
ObjFunction* compile(const char* source);
void markCompilerRoots();

#endif // include guard
