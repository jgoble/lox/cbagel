#include "common.h"

void initChunk(Chunk* chunk) {
    chunk->count = 0;
    chunk->capacity = 0;
    chunk->linecount = 0;
    chunk->linecapacity = 0;
    chunk->code = NULL;
    chunk->lines = NULL;
    chunk->lineruns = NULL;
    initValueArray(&chunk->constants);
    initTable(&chunk->constantTable);
}

void freeChunk(Chunk* chunk) {
    FREE_ARRAY(uint8_t, chunk->code, chunk->capacity);
    FREE_ARRAY(int, chunk->lines, chunk->linecapacity);
    FREE_ARRAY(int, chunk->lineruns, chunk->linecapacity);
    freeValueArray(&(chunk->constants));
    freeTable(&chunk->constantTable);
    initChunk(chunk);
}

void writeABCf(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c, bool f) {
    Instruction i;
    set_opf(i, o, f);
    set_A(i, a);
    set_B(i, b);
    set_C(i, c);
    writeInstruction(chunk, line, i);
}

void writeA(Chunk* chunk, int line, OpCodeReg o, u8 a) {
    writeABC(chunk, line, o, a, 0, 0);
}

void writeAflag(Chunk* chunk, int line, OpCodeReg o, u8 a, bool f) {
    writeABCf(chunk, line, o, a, 0, 0, f);
}

void writeAB(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b) {
    writeABC(chunk, line, o, a, b, 0);
}

void writeABC(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c) {
    writeABCf(chunk, line, o, a, b, c, false);
}

void writeABCX(Chunk* chunk, int line, OpCodeReg o, u8 a, u8 b, u8 c,
               u32 extra) {
    assert(extra <= 0xFFFFFF);
    writeABC(chunk, line, o, a, b, c);
    writeAx(chunk, line, OPR_EXTRAARG, extra);
}

void writeAeBx(Chunk* chunk, int line, OpCodeReg o, u8 a, u32 ebx) {
    Instruction i;
    set_A(i, a);
    if (ebx <= 0xFFFF) {
        set_opf(i, o, false);
        set_Bx(i, ebx);
        writeInstruction(chunk, line, i);
    } else {
        assert(ebx <= 0xFFFFFF || ebx >= 0xFF800000);
        set_opf(i, o, true);
        set_B(i, 0);
        set_C(i, 0);
        writeInstruction(chunk, line, i);
        writeAx(chunk, line, OPR_EXTRAARG, ebx);
    }
}

void writeAsBx(Chunk* chunk, int line, OpCodeReg o, u8 a, i16 sbx) {
    Instruction i;
    set_op(i, o);
    set_A(i, a);
    set_Bx(i, sbx);
    writeInstruction(chunk, line, i);
}

void writeAx(Chunk* chunk, int line, OpCodeReg o, u32 ax) {
    assert(ax <= 0xFFFFFF || ax >= 0xFF800000);
    Instruction i;
    set_op(i, o);
    set_Ax(i, ax);
    writeInstruction(chunk, line, i);
}

void writesAx(Chunk* chunk, int line, OpCodeReg o, i32 sax) {
    assert(sax >= -0x800000);
    assert(sax <= 0x7FFFFF);
    Instruction i;
    set_op(i, o);
    set_Ax(i, sax);
    writeInstruction(chunk, line, i);
}

void writeInstruction(Chunk* chunk, int line, Instruction i) {
    if (chunk->capacity < chunk->count + 1) {
        int oldCapacity = chunk->capacity;
        chunk->capacity = GROW_CAPACITY(oldCapacity);
        chunk->code = GROW_ARRAY(Instruction, chunk->code, oldCapacity,
                                 chunk->capacity);
    }
    chunk->code[chunk->count] = i;
    chunk->count++;

    if (chunk->lines == NULL) {
        chunk->linecapacity = GROW_CAPACITY(0);
        chunk->lines = GROW_ARRAY(int, chunk->lines, 0, chunk->linecapacity);
        chunk->lineruns
            = GROW_ARRAY(int, chunk->lineruns, 0, chunk->linecapacity);
    }

    if (chunk->lines[chunk->linecount - 1] == line) {
        chunk->lineruns[chunk->linecount - 1] += 1;
    } else {
        if (chunk->linecapacity < chunk->linecount + 1) {
            int oldCapacity = chunk->linecapacity;
            chunk->linecapacity = GROW_CAPACITY(oldCapacity);
            chunk->lines = GROW_ARRAY(int, chunk->lines, oldCapacity,
                                      chunk->linecapacity);
            chunk->lineruns = GROW_ARRAY(int, chunk->lineruns, oldCapacity,
                                         chunk->linecapacity);
        }
        chunk->lines[chunk->linecount] = line;
        chunk->lineruns[chunk->linecount] = 1;
        chunk->linecount++;
    }
}

int addConstant(Chunk* chunk, BagelValue value) {
    int newIndex = chunk->constants.count;
    pushTemp(value);
    if (IS_STRING(value)) {
        BagelValue index;
        if (tableGet(&chunk->constantTable, AS_STRING(value), &index)) {
            popTemp(1);
            return (int)AS_NUMBER(index);
        } else {
            tableSet(&chunk->constantTable, AS_STRING(value),
                     NUMBER_VAL(newIndex));
        }
    }
    writeValueArray(&(chunk->constants), value);
    popTemp(1);
    return newIndex;
}

int getLine(Chunk* chunk, int codeindex) {
    int instructions = -1;
    int i;
    for (i = 0; i < chunk->linecount; i++) {
        instructions += chunk->lineruns[i];
        if (instructions >= codeindex) {
            return chunk->lines[i];
        }
    }
    return 0;
}
