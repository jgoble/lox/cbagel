#include "common.h"

typedef struct {
    Token current;
    Token previous;
    bool hadError;
    bool panicMode;
} Parser;

typedef enum
{
    PREC_NONE,
    PREC_ASSIGNMENT, // =
    PREC_TERNARY, // ?:
    PREC_OR, // or
    PREC_AND, // and
    PREC_EQUALITY, // == !=
    PREC_COMPARISON, // < > <= >=
    PREC_TERM, // + -
    PREC_FACTOR, // * /
    PREC_UNARY, // ! -
    PREC_EXPONENT, // ^
    PREC_CALL, // . ()
    PREC_PRIMARY
} Precedence;

typedef u8 (*InfixFn)(bool canAssign, u8 left);
typedef u8 (*ParseFn)(bool canAssign);

typedef struct {
    ParseFn prefix;
    InfixFn infix;
    Precedence precedence;
} ParseRule;

typedef struct {
    Token name;
    int depth;
    bool isCaptured;
} Local;

typedef enum
{
    TYPE_FUNCTION,
    TYPE_INITIALIZER,
    TYPE_METHOD,
    TYPE_SCRIPT
} FunctionType;

typedef struct {
    int scopeDepth;
    int loopStart;
    int exitCount;
    int exitJumps[UINT8_COUNT];
} LoopInfo;

typedef struct Compiler {
    struct Compiler* enclosing;
    ObjFunction* function;
    FunctionType type;
    Local locals[UINT8_COUNT];
    int localCount;
    Upvalue upvalues[UINT8_COUNT];
    int scopeDepth;
    LoopInfo* loop;
    int stack;
} Compiler;

typedef struct ClassCompiler {
    struct ClassCompiler* enclosing;
    Token name;
    bool hasSuperclass;
    ObjClass* klass;
} ClassCompiler;

Parser parser;

Compiler* current = NULL;

ClassCompiler* currentClass = NULL;

static Chunk* currentChunk() {
    return &current->function->chunk;
}

static void errorAt(Token* token, const char* message) {
    if (parser.panicMode) {
        return;
    }
    parser.panicMode = true;

    fprintf(stderr, "[line %d] Error", token->line);

    if (token->type == TOKEN_EOF) {
        fprintf(stderr, " at end");
    } else if (token->type == TOKEN_ERROR) {
        // Nothing.
    } else {
        fprintf(stderr, " at '%.*s'", token->length, token->start);
    }

    fprintf(stderr, ": %s\n", message);
    parser.hadError = true;
}

void error(const char* message) {
    errorAt(&parser.previous, message);
}

static void errorAtCurrent(const char* message) {
    errorAt(&parser.current, message);
}

static void advance() {
    parser.previous = parser.current;

    for (;;) {
        parser.current = scanToken();
        if (parser.current.type != TOKEN_ERROR) {
            break;
        }
        errorAtCurrent(parser.current.start);
    }
}

static void consume(TokenType type, const char* message) {
    if (parser.current.type == type) {
        advance();
        return;
    }

    errorAtCurrent(message);
}

static bool check(TokenType type) {
    return parser.current.type == type;
}

static bool match(TokenType type) {
    if (!check(type)) {
        return false;
    }
    advance();
    return true;
}

static bool isLocal(u8 reg) {
    return reg < current->localCount;
}

static u8 push() {
    if (current->stack == MAXREG) {
        error("Function or expression needs too many registers.");
        return 0;
    }
    return ++current->stack;
}

static void pop(u8 i) {
    if (!isLocal(i) && !parser.hadError) {
        assert(i == current->stack);
        current->stack--;
    }
}

#define emit(type, o, ...) \
    write##type(currentChunk(), parser.previous.line, o, __VA_ARGS__)

static u8 pushIfLocal(u8 reg) {
    if (isLocal(reg)) {
        u8 result = push();
        emit(AB, OPR_MOVE, result, reg);
        return result;
    } else {
        return reg;
    }
}

static void emitLoop(int loopStart) {
    int offset = currentChunk()->count - loopStart + 1;
    if (offset > MAXJUMP) {
        error("Loop body too large.");
    }

    emit(sAx, OPR_JUMP, -offset);
}

static int emitJump(void) {
    emit(sAx, OPR_JUMP, PLACEHOLDER);
    return currentChunk()->count;
}

static int emitConditionalJump(u8 i, u8 reg) {
    emit(AeBx, i, reg, PLACEHOLDER);
    return currentChunk()->count; // EXTRAARG
}

static void emitReturn() {
    if (current->type == TYPE_INITIALIZER) {
        emit(Aflag, OPR_RETURN, 0, true);
    } else {
        emit(Aflag, OPR_RETURN, 0, false);
    }
}

static u8 getRKvalue(int constant, bool* isK) {
    if (constant > UINT8_MAX) {
        int result = push();
        emit(AeBx, OPR_LOADK, result, constant);
        *isK = false;
        return result;
    } else {
        *isK = true;
        return constant;
    }
}

static int makeConstant(BagelValue value) {
    int constant = addConstant(currentChunk(), value);
    if (constant > MAXCONSTANT) {
        error("Too many constants in one chunk.");
        return 0;
    }

    return constant;
}

static void emitConstant(BagelValue value, u8 reg) {
    emit(AeBx, OPR_LOADK, reg, makeConstant(value));
}

static void patchJump(int offset) {
    int jump = currentChunk()->count - offset;

    if (jump > MAXJUMP) {
        error("Too much code to jump over.");
    }

    set_Ax(currentChunk()->code[offset - 1], jump);
}

static void patchJumps() {
    for (int j = 0; j < current->loop->exitCount; j++) {
        int offset = current->loop->exitJumps[j];
        patchJump(offset);
    }
}

static void initLoopInfo(LoopInfo* loop, int loopStart) {
    loop->scopeDepth = current->scopeDepth;
    loop->loopStart = loopStart;
    loop->exitCount = 0;
}

static void initCompiler(Compiler* compiler, FunctionType type) {
    compiler->enclosing = current;
    compiler->function = NULL;
    compiler->type = type;
    compiler->localCount = 0;
    compiler->scopeDepth = 0;
    compiler->loop = NULL;
    compiler->stack = 0;
    compiler->function = newFunction();
    current = compiler;

    if (type != TYPE_SCRIPT) {
        current->function->name
            = copyString(parser.previous.start, parser.previous.length);
    }

    Local* local = &current->locals[current->localCount++];
    local->depth = 0;
    local->isCaptured = false;
    if (type != TYPE_FUNCTION) {
        local->name.start = "this";
        local->name.length = 4;
    } else {
        local->name.start = "";
        local->name.length = 0;
    }
}

static ObjFunction* endCompiler() {
    emitReturn();
    ObjFunction* function = current->function;

#ifdef DEBUG_PRINT_CODE
    if (!parser.hadError) {
        disassembleChunk(currentChunk(), function->name != NULL
                                             ? function->name->chars
                                             : "<script>");
    }
#endif

    current = current->enclosing;
    return function;
}

static void beginScope() {
    current->scopeDepth++;
}

static void endScope(bool allowClose) {
    bool close = false;
    current->scopeDepth--;

    while (current->localCount > 0
           && current->locals[current->localCount - 1].depth
                  > current->scopeDepth) {
        if (current->locals[current->localCount - 1].isCaptured) {
            close = true;
        }
        current->localCount--;
        pop(current->localCount);
    }

    if (close && allowClose) {
        emit(A, OPR_CLOSEUPVAL, current->localCount);
    }
}

static u8 expression();
static u8 parsePrecedence(Precedence precedence);
static void statement();
static void declaration();
static ParseRule* getRule(TokenType type);

static int identifierConstant(Token* name) {
    return makeConstant(OBJ_VAL(copyString(name->start, name->length)));
}

static bool identifiersEqual(Token* a, Token* b) {
    if (a->length != b->length) {
        return false;
    }
    return memcmp(a->start, b->start, a->length) == 0;
}

static int resolveLocal(Compiler* compiler, Token* name) {
    for (int i = compiler->localCount - 1; i >= 0; i--) {
        Local* local = &compiler->locals[i];
        if (identifiersEqual(name, &local->name)) {
            if (local->depth == -1) {
                error("Can't read local variable in its own initializer.");
            }
            return i;
        }
    }
    return -1;
}

static int addUpvalue(Compiler* compiler, uint8_t index, bool isLocal) {
    int upvalueCount = compiler->function->upvalueCount;

    for (int i = 0; i < upvalueCount; i++) {
        Upvalue* upvalue = &compiler->upvalues[i];
        if (upvalue->index == index && upvalue->isLocal == isLocal) {
            return i;
        }
    }
    if (upvalueCount == UINT8_COUNT) {
        error("Too many closure variables in function.");
    }

    compiler->upvalues[upvalueCount].isLocal = isLocal;
    compiler->upvalues[upvalueCount].index = index;
    return compiler->function->upvalueCount++;
}

static int resolveUpvalue(Compiler* compiler, Token* name) {
    if (compiler->enclosing == NULL) {
        return -1;
    }
    int local = resolveLocal(compiler->enclosing, name);
    if (local != -1) {
        compiler->enclosing->locals[local].isCaptured = true;
        return addUpvalue(compiler, (uint8_t)local, true);
    }
    int upvalue = resolveUpvalue(compiler->enclosing, name);
    if (upvalue != -1) {
        return addUpvalue(compiler, (uint8_t)upvalue, false);
    }
    return -1;
}

static void addLocal(Token name) {
    if (current->localCount == UINT8_COUNT) {
        error("Too many local variables in function.");
    }
    Local* local = &current->locals[current->localCount++];
    local->name = name;
    local->depth = -1;
    local->isCaptured = false;
}

static void declareVariable() {
    if (current->scopeDepth == 0) {
        return;
    }

    Token* name = &parser.previous;
    for (int i = current->localCount - 1; i >= 0; i--) {
        Local* local = &current->locals[i];
        if (local->depth != -1 && local->depth < current->scopeDepth) {
            break;
        }
        if (identifiersEqual(name, &local->name)) {
            error("Already variable with this name in this scope.");
        }
    }
    addLocal(*name);
}

static int parseVariable(const char* errorMessage) {
    consume(TOKEN_IDENTIFIER, errorMessage);

    declareVariable();
    if (current->scopeDepth > 0) {
        return 0;
    }
    return identifierConstant(&parser.previous);
}

static void markInitialized() {
    if (current->scopeDepth == 0) {
        return;
    }
    current->locals[current->localCount - 1].depth = current->scopeDepth;
}

static void defineVariable(int global, int reg) {
    if (current->scopeDepth > 0) {
        markInitialized();
        if (reg >= 0) {
            emit(AB, OPR_MOVE, current->localCount - 1, reg);
            pop(reg);
        }
    } else if (reg >= 0) {
        emit(AeBx, OPR_DEFGLOBAL, reg, global);
        pop(reg);
    }
}

static u8 argumentList() {
    u8 argCount = 0;
    if (!check(TOKEN_RIGHT_PAREN)) {
        do {
            pushIfLocal(expression());
            argCount++;
        } while (match(TOKEN_COMMA));
    }
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after arguments.");
    return argCount;
}

static u8 and (bool canAssign, u8 left) {
    left = pushIfLocal(left);
    int endJump = emitConditionalJump(OPR_JUMPONFALSE, left);
    pop(left);

    u8 right = parsePrecedence(PREC_AND);
    right = pushIfLocal(right);
    patchJump(endJump);
    assert(left == right);

    return right;
}

static u8 binary(bool canAssign, u8 left) {
    TokenType operatorType = parser.previous.type;

    // Compile the right operand
    ParseRule* rule = getRule(operatorType);
    u8 right;
    if (operatorType == TOKEN_CARET) {
        // Exponentiation is right-associative
        right = parsePrecedence(rule->precedence);
    } else {
        // All other binary operations are left-associative
        right = parsePrecedence((Precedence)(rule->precedence + 1));
    }

    OpCodeReg opcode;
    // Emit the operator instruction
    switch (operatorType) {
        case TOKEN_BANG_EQUAL:
            opcode = OPR_NE;
            break;
        case TOKEN_EQUAL_EQUAL:
            opcode = OPR_EQ;
            break;
        case TOKEN_GREATER:
            opcode = OPR_GT;
            break;
        case TOKEN_GREATER_EQUAL:
            opcode = OPR_GE;
            break;
        case TOKEN_LESS:
            opcode = OPR_LT;
            break;
        case TOKEN_LESS_EQUAL:
            opcode = OPR_LE;
            break;
        case TOKEN_PLUS:
            opcode = OPR_ADD;
            break;
        case TOKEN_MINUS:
            opcode = OPR_SUB;
            break;
        case TOKEN_STAR:
            opcode = OPR_MUL;
            break;
        case TOKEN_SLASH:
            opcode = OPR_DIV;
            break;
        case TOKEN_PERCENT:
            opcode = OPR_MOD;
            break;
        case TOKEN_CARET:
            opcode = OPR_POW;
            break;
        default:
            error("Internal parser error (binary)");
            return 0;
    }
    pop(right);
    pop(left);
    u8 result = push();
    emit(ABC, opcode, result, left, right);
    return result;
}

static u8 call(bool canAssign, u8 left) {
    u8 func = pushIfLocal(left);
    u8 argCount = argumentList();
    int top = func + argCount;
    if (!parser.hadError) {
        assert(top <= MAXREG);
    }
    emit(AB, OPR_CALL, func, argCount);
    while (top > func) {
        pop(top--);
    }
    return func;
}

static u8 dot(bool canAssign, u8 left) {
    consume(TOKEN_IDENTIFIER, "Expect property name after '.'.");
    int name = identifierConstant(&parser.previous);

    left = pushIfLocal(left);
    u8 b, c;
    bool isK;
    if (canAssign && match(TOKEN_EQUAL)) {
        c = expression();
        b = getRKvalue(name, &isK);
        emit(ABCf, OPR_SETATTR, left, b, c, isK);
        if (!isK) {
            pop(b);
        }
        pop(c); // VM moves RC to left
        return left;
    } else if (match(TOKEN_LEFT_PAREN)) {
        c = argumentList();
        int top = left + c;
        if (!parser.hadError) {
            assert(top <= MAXREG);
        }
        b = getRKvalue(name, &isK);
        emit(ABCf, OPR_INVOKE, left, b, c, isK);
        if (!isK) {
            pop(b);
        }
        while (top > left) {
            pop(top--);
        }
        return left;
    } else {
        emit(AeBx, OPR_GETATTR, left, name);
        return left;
    }
}

static u8 literal(bool canAssign) {
    u8 result = push();
    switch (parser.previous.type) {
        case TOKEN_NIL:
            emit(A, OPR_LOADNIL, result);
            break;
        case TOKEN_FALSE:
            emit(AB, OPR_LOADBOOL, result, 0);
            break;
        case TOKEN_TRUE:
            emit(AB, OPR_LOADBOOL, result, 1);
            break;
        default:
            error("Internal parser error (literal)");
            return 0;
    }
    return result;
}

static u8 grouping(bool canAssign) {
    u8 result = expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after expression.");
    return result;
}

static u8 ternary(bool canAssign, u8 left) {
    // We know we have ? here
    int thenJump = emitConditionalJump(OPR_JUMPONFALSE, left);
    pop(left);

    // Matching C and C++, the middle expression is treated as if
    // it were parenthesized
    u8 middle = expression();
    middle = pushIfLocal(middle);

    int elseJump = emitJump();
    patchJump(thenJump);
    consume(TOKEN_COLON, "Expect ':' to continue ternary expression");
    pop(middle);

    // Matching C, C++, and Java, the ternary operator is right-associative
    u8 right = parsePrecedence(PREC_TERNARY);
    right = pushIfLocal(right);
    assert(parser.hadError || middle == right);

    patchJump(elseJump);
    return right;
}

static u8 number(bool canAssign) {
    double value = strtod(parser.previous.start, NULL);
    u8 result = push();
    if (value >= INT16_MIN && value <= INT16_MAX
        && value == nearbyint(value)) {
        emit(AsBx, OPR_LOADI, result, (i16)value);
    } else {
        emitConstant(NUMBER_VAL(value), result);
    }
    return result;
}

static u8 or (bool canAssign, u8 left) {
    left = pushIfLocal(left);
    int elseJump = emitConditionalJump(OPR_JUMPONFALSE, left);
    int endJump = emitJump();

    patchJump(elseJump);
    pop(left);

    u8 right = parsePrecedence(PREC_OR);
    right = pushIfLocal(right);
    assert(left == right);

    patchJump(endJump);
    return right;
}

static u8 string(bool canAssign) {
    u8 result = push();
    ObjString* string
        = copyString(parser.previous.start + 1, parser.previous.length - 2);
    emitConstant(OBJ_VAL(string), result);
    return result;
}

static u8 formatString(bool canAssign) {
    u8 stringParts = 0;
    u8 stringBuilds = 0;
    int ra = push();
    pop(push()); // need at least two registers free
    pop(ra);
    for (;;) {
        switch (parser.current.type) {
            case TOKEN_STRING_PART:
                if (parser.current.length > 0) {
                    ObjString* str = copyString(parser.current.start,
                                                parser.current.length);
                    emitConstant(OBJ_VAL(str), push());
                    stringParts++;
                }
                advance();
                break;
            case TOKEN_LEFT_BRACE:
                advance();
                expression();
                stringParts++;
                consume(TOKEN_RIGHT_BRACE, "Unterminated format expression.");
                break;
            case TOKEN_FORMAT_END:
                if (stringParts > 0) {
                    emit(AB, OPR_BUILDSTR, ra, stringParts);
                }
                if (stringBuilds > 0) {
                    ra -= stringBuilds;
                    emit(AB, OPR_BUILDSTR, ra, stringBuilds + 1);
                }
                advance();
                assert(current->stack >= ra);
                current->stack = ra;
                return ra;
            default:
                errorAtCurrent("Internal parser error (formatString)");
                return 0;
        }
        if (current->stack == MAXREG) {
            if (stringParts == 1) {
                ra -= stringBuilds;
                stringParts = stringBuilds + 1;
                stringBuilds = 0;
            }
            emit(AB, OPR_BUILDSTR, ra, stringParts);
            stringParts = 0;
            stringBuilds++;
            current->stack = ra;
            ra++;
        }
    }
}

static u8 namedVariable(Token name, bool canAssign) {
    int arg = resolveLocal(current, &name);
    if (arg != -1) {
        if (canAssign && match(TOKEN_EQUAL)) {
            u8 value = expression();
            emit(AB, OPR_MOVE, arg, value);
            pop(value);
        }
        return arg;
    } else if ((arg = resolveUpvalue(current, &name)) != -1) {
        if (canAssign && match(TOKEN_EQUAL)) {
            u8 value = expression();
            emit(AB, OPR_SETUPVAL, value, arg);
            return value;
        } else {
            u8 result = push();
            emit(AB, OPR_GETUPVAL, result, arg);
            return result;
        }
    } else {
        arg = identifierConstant(&name);
        if (canAssign && match(TOKEN_EQUAL)) {
            u8 result = expression();
            emit(AeBx, OPR_SETGLOBAL, result, arg);
            return result;
        } else {
            u8 result = push();
            emit(AeBx, OPR_GETGLOBAL, result, arg);
            return result;
        }
    }
}

static u8 variable(bool canAssign) {
    return namedVariable(parser.previous, canAssign);
}

static Token syntheticToken(const char* text) {
    Token token;
    token.start = text;
    token.length = (int)strlen(text);
    return token;
}

static u8 super_(bool canAssign) {
    if (currentClass == NULL) {
        error("Can't use 'super' outside of a class.");
    } else if (!currentClass->hasSuperclass) {
        error("Can't use 'super' in a class with no superclass.");
    }
    consume(TOKEN_DOT, "Expect '.' after 'super'.");
    consume(TOKEN_IDENTIFIER, "Expect superclass method name.");
    int name = identifierConstant(&parser.previous);

    u8 ra = push();
    emit(AB, OPR_MOVE, ra, 0); // this
    Token superName = syntheticToken("super");
    u8 superUpval = resolveUpvalue(current, &superName);
    if (match(TOKEN_LEFT_PAREN)) {
        u8 argCount = argumentList();
        int top = ra + argCount;
        assert(top <= MAXREG);
        emit(ABCX, OPR_SUPERINVOKE, ra, superUpval, argCount, name);
        while (top > ra) {
            pop(top--);
        }
    } else {
        bool isK;
        int b = getRKvalue(name, &isK);
        emit(ABCf, OPR_GETSUPER, ra, b, superUpval, isK);
        if (!isK) {
            pop(b);
        }
    }
    return ra;
}

static u8 this_(bool canAssign) {
    if (currentClass == NULL) {
        error("Can't use 'this' outside of a class.");
        return 0;
    }
    return variable(false);
}

static u8 unary(bool canAssign) {
    TokenType operatorType = parser.previous.type;

    // Compile the operand
    u8 right = parsePrecedence(PREC_UNARY);
    u8 left = isLocal(right) ? push() : right;

    // Emit the operator instruction
    switch (operatorType) {
        case TOKEN_BANG:
            emit(AB, OPR_NOT, left, right);
            break;
        case TOKEN_MINUS:
            emit(AB, OPR_NEG, left, right);
            break;
        default:
            error("Internal parser error (unary)");
            return 0;
    }

    return left;
}

ParseRule rules[] = {
    [TOKEN_LEFT_PAREN] = {grouping, call, PREC_CALL},
    [TOKEN_RIGHT_PAREN] = {NULL, NULL, PREC_NONE},
    [TOKEN_LEFT_BRACE] = {NULL, NULL, PREC_NONE},
    [TOKEN_RIGHT_BRACE] = {NULL, NULL, PREC_NONE},
    [TOKEN_CARET] = {NULL, binary, PREC_EXPONENT},
    [TOKEN_COLON] = {NULL, NULL, PREC_NONE},
    [TOKEN_COMMA] = {NULL, NULL, PREC_NONE},
    [TOKEN_DOT] = {NULL, dot, PREC_CALL},
    [TOKEN_MINUS] = {unary, binary, PREC_TERM},
    [TOKEN_PERCENT] = {NULL, binary, PREC_FACTOR},
    [TOKEN_PLUS] = {NULL, binary, PREC_TERM},
    [TOKEN_QUESTION] = {NULL, ternary, PREC_TERNARY},
    [TOKEN_SEMICOLON] = {NULL, NULL, PREC_NONE},
    [TOKEN_SLASH] = {NULL, binary, PREC_FACTOR},
    [TOKEN_STAR] = {NULL, binary, PREC_FACTOR},
    [TOKEN_BANG] = {unary, NULL, PREC_NONE},
    [TOKEN_BANG_EQUAL] = {NULL, binary, PREC_EQUALITY},
    [TOKEN_EQUAL] = {NULL, NULL, PREC_NONE},
    [TOKEN_EQUAL_EQUAL] = {NULL, binary, PREC_EQUALITY},
    [TOKEN_GREATER] = {NULL, binary, PREC_COMPARISON},
    [TOKEN_GREATER_EQUAL] = {NULL, binary, PREC_COMPARISON},
    [TOKEN_LESS] = {NULL, binary, PREC_COMPARISON},
    [TOKEN_LESS_EQUAL] = {NULL, binary, PREC_COMPARISON},
    [TOKEN_IDENTIFIER] = {variable, NULL, PREC_NONE},
    [TOKEN_STRING] = {string, NULL, PREC_NONE},
    [TOKEN_STRING_PART] = {NULL, NULL, PREC_NONE},
    [TOKEN_FORMAT_START] = {formatString, NULL, PREC_NONE},
    [TOKEN_FORMAT_END] = {NULL, NULL, PREC_NONE},
    [TOKEN_NUMBER] = {number, NULL, PREC_NONE},
    [TOKEN_AND] = {NULL, and, PREC_AND},
    [TOKEN_BREAK] = {NULL, NULL, PREC_AND},
    [TOKEN_CLASS] = {NULL, NULL, PREC_NONE},
    [TOKEN_CONTINUE] = {NULL, NULL, PREC_AND},
    [TOKEN_ELSE] = {NULL, NULL, PREC_NONE},
    [TOKEN_FALSE] = {literal, NULL, PREC_NONE},
    [TOKEN_FOR] = {NULL, NULL, PREC_NONE},
    [TOKEN_FUN] = {NULL, NULL, PREC_NONE},
    [TOKEN_IF] = {NULL, NULL, PREC_NONE},
    [TOKEN_NIL] = {literal, NULL, PREC_NONE},
    [TOKEN_OR] = {NULL, or, PREC_OR},
    [TOKEN_PRINT] = {NULL, NULL, PREC_NONE},
    [TOKEN_RETURN] = {NULL, NULL, PREC_NONE},
    [TOKEN_SUPER] = {super_, NULL, PREC_NONE},
    [TOKEN_THIS] = {this_, NULL, PREC_NONE},
    [TOKEN_TRUE] = {literal, NULL, PREC_NONE},
    [TOKEN_VAR] = {NULL, NULL, PREC_NONE},
    [TOKEN_WHILE] = {NULL, NULL, PREC_NONE},
    [TOKEN_ERROR] = {NULL, NULL, PREC_NONE},
    [TOKEN_EOF] = {NULL, NULL, PREC_NONE},
};

static u8 parsePrecedence(Precedence precedence) {
    advance();
    ParseFn prefixRule = getRule(parser.previous.type)->prefix;
    if (prefixRule == NULL) {
        error("Expect expression.");
        return 0;
    }

    bool canAssign = precedence <= PREC_ASSIGNMENT;
    u8 result = prefixRule(canAssign);

    while (precedence <= getRule(parser.current.type)->precedence) {
        advance();
        InfixFn infixRule = getRule(parser.previous.type)->infix;
        result = infixRule(canAssign, result);
    }

    if (canAssign && match(TOKEN_EQUAL)) {
        error("Invalid assignment target.");
    }
    return result;
}

static ParseRule* getRule(TokenType type) {
    return &rules[type];
}

static u8 expression() {
    return parsePrecedence(PREC_ASSIGNMENT);
}

static void block() {
    while (!check(TOKEN_RIGHT_BRACE) && !check(TOKEN_EOF)) {
        declaration();
    }

    consume(TOKEN_RIGHT_BRACE, "Expect '}' after block.");
}

static ObjFunction* rawFunction(FunctionType type) {
    Compiler compiler;
    initCompiler(&compiler, type);
    beginScope();

    // Compile the parameter list
    consume(TOKEN_LEFT_PAREN, "Expect '(' after function name.");
    if (!check(TOKEN_RIGHT_PAREN)) {
        do {
            current->function->arity++;
            if (current->function->arity > 255) {
                errorAtCurrent("Can't have more than 255 parameters.");
            }
            push();
            int paramConstant = parseVariable("Expect parameter name.");
            defineVariable(paramConstant, -1);
        } while (match(TOKEN_COMMA));
    }
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after parameters.");

    // The body
    consume(TOKEN_LEFT_BRACE, "Expect '{' before function body.");
    block();

    // Create the function object
    ObjFunction* func = endCompiler();
    pushTemp(OBJ_VAL(func));
    func->upvalueInfo = ALLOCATE(Upvalue, func->upvalueCount);
    popTemp(1);
    for (int i = 0; i < func->upvalueCount; i++) {
        func->upvalueInfo[i].isLocal = compiler.upvalues[i].isLocal;
        func->upvalueInfo[i].index = compiler.upvalues[i].index;
    }

    return func;
}

static u8 function(FunctionType type) {
    ObjFunction* func = rawFunction(type);
    u8 reg = push();
    emit(AeBx, OPR_CLOSURE, reg, makeConstant(OBJ_VAL(func)));
    return reg;
}

static void method() {
    consume(TOKEN_IDENTIFIER, "Expect method name.");
    BagelValue name
        = OBJ_VAL(copyString(parser.previous.start, parser.previous.length));
    pushTemp(name);

    FunctionType type = TYPE_METHOD;
    if (parser.previous.length == 4
        && memcmp(parser.previous.start, "init", 4) == 0) {
        type = TYPE_INITIALIZER;
    }
    BagelValue func = OBJ_VAL(rawFunction(type));
    pushTemp(func);
    writeMethodArray(&currentClass->klass->rawMethods, name, func);
    popTemp(2);
}

static void classDeclaration() {
    consume(TOKEN_IDENTIFIER, "Expect class name.");
    Token className = parser.previous;
    int nameConstant = identifierConstant(&parser.previous);

    u8 ra, rb, rc;
    ra = 0;
    bool isK;
    declareVariable();
    if (current->scopeDepth > 0) {
        ra = push();
        markInitialized();
    }

    ClassCompiler classCompiler;
    classCompiler.name = parser.previous;
    classCompiler.hasSuperclass = false;
    classCompiler.enclosing = currentClass;
    classCompiler.klass
        = newClass(AS_STRING(currentChunk()->constants.values[nameConstant]));
    currentClass = &classCompiler;

    int classConstant = makeConstant(OBJ_VAL(classCompiler.klass));
    rb = getRKvalue(classConstant, &isK);
    rc = NOREG;

    if (match(TOKEN_LESS)) {
        consume(TOKEN_IDENTIFIER, "Expect superclass name.");
        rc = pushIfLocal(variable(false));

        if (identifiersEqual(&className, &parser.previous)) {
            error("A class can't inherit from itself.");
        }

        beginScope();
        addLocal(syntheticToken("super"));
        markInitialized();
        classCompiler.hasSuperclass = true;
    }

    consume(TOKEN_LEFT_BRACE, "Expect '{' before class body.");
    while (!check(TOKEN_RIGHT_BRACE) && !check(TOKEN_EOF)) {
        method();
    }
    consume(TOKEN_RIGHT_BRACE, "Expect '}' after class body.");

    if (classCompiler.hasSuperclass) {
        endScope(false); // pops rc
    }

    if (!isK) {
        pop(rb);
    }

    if (ra == 0) {
        ra = push();
    }

    emit(ABCf, OPR_CLASS, ra, rb, rc, isK);
    if (current->scopeDepth == 0) {
        emit(AeBx, OPR_DEFGLOBAL, ra, nameConstant);
    }
    pop(ra);

    currentClass = currentClass->enclosing;
}

static void funDeclaration() {
    int name = parseVariable("Expect function name.");
    markInitialized();
    u8 ra = function(TYPE_FUNCTION);
    defineVariable(name, ra);
}

static void varDeclaration() {
    int name = parseVariable("Expect variable name.");

    u8 value;
    if (match(TOKEN_EQUAL)) {
        value = pushIfLocal(expression());
    } else {
        value = push();
        emit(A, OPR_LOADNIL, value);
    }
    consume(TOKEN_SEMICOLON, "Expect ';' after variable declaration.");

    defineVariable(name, value);
}

static void expressionStatement() {
    u8 result = expression();
    consume(TOKEN_SEMICOLON, "Expect ';' after expression.");
    pop(result);
}

static void breakStatement() {
    if (current->loop == NULL) {
        error("Can't have a 'break' outside of a loop.");
        return;
    }
    current->loop->exitJumps[current->loop->exitCount++] = emitJump();
    consume(TOKEN_SEMICOLON, "Expect ';' after 'break'.");
}

static void continueStatement() {
    if (current->loop == NULL) {
        error("Can't have a 'continue' outside of a loop.");
        return;
    }
    emitLoop(current->loop->loopStart);
    consume(TOKEN_SEMICOLON, "Expect ';' after 'continue'.");
}

static void forStatement() {
    beginScope();

    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'for'.");
    if (match(TOKEN_SEMICOLON)) {
        // No initializer
    } else if (match(TOKEN_VAR)) {
        varDeclaration();
    } else {
        expressionStatement();
    }

    LoopInfo loop;
    LoopInfo* outerLoop = current->loop;
    current->loop = &loop;
    initLoopInfo(&loop, currentChunk()->count);

    int exitJump = -1;
    if (!match(TOKEN_SEMICOLON)) {
        u8 condition = expression();
        consume(TOKEN_SEMICOLON, "Expect ';' after loop condition.");

        // Jump out of the loop if the condition is false
        exitJump = emitConditionalJump(OPR_JUMPONFALSE, condition);
        pop(condition);
    }

    if (!match(TOKEN_RIGHT_PAREN)) {
        int bodyJump = emitJump();

        int incrementStart = currentChunk()->count;
        pop(expression());
        consume(TOKEN_RIGHT_PAREN, "Expect ')' after for clauses.");

        emitLoop(loop.loopStart);
        loop.loopStart = incrementStart;
        patchJump(bodyJump);
    }

    statement();

    emitLoop(loop.loopStart);

    if (exitJump != -1) {
        patchJump(exitJump);
    }

    patchJumps();
    current->loop = outerLoop;
    endScope(true);
}

static void ifStatement() {
    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'if'.");
    u8 condition = expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

    int thenJump = emitConditionalJump(OPR_JUMPONFALSE, condition);
    pop(condition);
    statement();

    int elseJump = emitJump();

    patchJump(thenJump);

    if (match(TOKEN_ELSE)) {
        statement();
    }
    patchJump(elseJump);
}

static void printStatement() {
    u8 result = expression();
    consume(TOKEN_SEMICOLON, "Expect ';' after value.");
    emit(A, OPR_PRINT, result);
    pop(result);
}

static void returnStatement() {
    if (current->type == TYPE_SCRIPT) {
        error("Can't return from top-level code.");
    }
    if (match(TOKEN_SEMICOLON)) {
        emitReturn();
    } else {
        if (current->type == TYPE_INITIALIZER) {
            error("Can't return a value from an initializer.");
        }
        u8 value = expression();
        consume(TOKEN_SEMICOLON, "Expect ';' after return value.");
        emit(Aflag, OPR_RETURN, value, true);
        pop(value);
    }
}

static void switchStatement() {
    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'switch'.");
    u8 ra = pushIfLocal(expression());
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after switch condition.");

    consume(TOKEN_LEFT_BRACE, "Expect '{' after switch expression.");

    int exitCount = 0;
    int exitJumps[UINT8_COUNT];
    int caseJump = -1;

    while (!check(TOKEN_DEFAULT) && !check(TOKEN_RIGHT_BRACE)
           && !check(TOKEN_EOF)) {
        consume(TOKEN_CASE, "Expect statement or 'case'.");
        if (caseJump != -1) {
            patchJump(caseJump);
        }
        u8 casevalue = pushIfLocal(expression());
        assert(ra + 1 == casevalue);
        consume(TOKEN_COLON, "Expect ':' after case expression.");
        caseJump = emitConditionalJump(OPR_CASE, ra);

        consume(TOKEN_LEFT_BRACE, "Expect block after case expression.");
        pop(casevalue);
        pop(ra);
        block();
#ifdef NDEBUG
        push();
#else
        u8 ra2 = push();
        assert(ra == ra2);
#endif

        exitJumps[exitCount++] = emitJump();
    }

    if (caseJump != -1) {
        patchJump(caseJump);
    }
    // Pop the original switch expression
    pop(ra);

    if (check(TOKEN_DEFAULT)) {
        advance();
        consume(TOKEN_COLON, "Expect ':' after 'default'.");
        consume(TOKEN_LEFT_BRACE, "Expect block after 'default'.");
        block();
    }

    consume(TOKEN_RIGHT_BRACE, "Expect '}' to close switch statement.");

    for (int j = 0; j < exitCount; j++) {
        patchJump(exitJumps[j]);
    }
}

static void whileStatement() {
    LoopInfo loop;
    LoopInfo* outerLoop = current->loop;
    current->loop = &loop;
    initLoopInfo(&loop, currentChunk()->count);

    consume(TOKEN_LEFT_PAREN, "Expect '(' after 'while'.");
    u8 condition = expression();
    consume(TOKEN_RIGHT_PAREN, "Expect ')' after condition.");

    int exitJump = emitConditionalJump(OPR_JUMPONFALSE, condition);
    pop(condition);

    statement();

    emitLoop(loop.loopStart);

    patchJump(exitJump);
    patchJumps();
    current->loop = outerLoop;
}

static void synchronize() {
    parser.panicMode = false;

    while (parser.current.type != TOKEN_EOF) {
        if (parser.previous.type == TOKEN_SEMICOLON) {
            return;
        }

        switch (parser.current.type) {
            case TOKEN_CLASS:
            case TOKEN_FUN:
            case TOKEN_VAR:
            case TOKEN_BREAK:
            case TOKEN_CONTINUE:
            case TOKEN_FOR:
            case TOKEN_IF:
            case TOKEN_WHILE:
            case TOKEN_PRINT:
            case TOKEN_RETURN:
                return;
            default:
                // Do nothing
                ;
        }

        advance();
    }
}

static void declaration() {
    if (match(TOKEN_CLASS)) {
        classDeclaration();
    } else if (match(TOKEN_FUN)) {
        funDeclaration();
    } else if (match(TOKEN_VAR)) {
        varDeclaration();
    } else {
        statement();
    }

    if (parser.panicMode) {
        synchronize();
    }
}

static void statement() {
    if (match(TOKEN_PRINT)) {
        printStatement();
    } else if (match(TOKEN_BREAK)) {
        breakStatement();
    } else if (match(TOKEN_CONTINUE)) {
        continueStatement();
    } else if (match(TOKEN_FOR)) {
        forStatement();
    } else if (match(TOKEN_IF)) {
        ifStatement();
    } else if (match(TOKEN_RETURN)) {
        returnStatement();
    } else if (match(TOKEN_SWITCH)) {
        switchStatement();
    } else if (match(TOKEN_WHILE)) {
        whileStatement();
    } else if (match(TOKEN_LEFT_BRACE)) {
        beginScope();
        block();
        endScope(true);
    } else {
        expressionStatement();
    }
}

ObjFunction* compile(const char* source) {
    initScanner(source);
    Compiler compiler;
    initCompiler(&compiler, TYPE_SCRIPT);

    parser.hadError = false;
    parser.panicMode = false;

    advance();

    while (!match(TOKEN_EOF)) {
        declaration();
    }

    ObjFunction* function = endCompiler();
    return parser.hadError ? NULL : function;
}

void markCompilerRoots() {
    Compiler* compiler = current;
    while (compiler != NULL) {
        markObject((Obj*)compiler->function);
        compiler = compiler->enclosing;
    }
}
