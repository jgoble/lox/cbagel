#ifndef bagel_bagel_h
#define bagel_bagel_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* Public API */

/***********************************************************************
   WARNING: The details of this struct are NOT PUBLIC. Consumers of
   this API must treat BagelValue as an opaque type. All access to
   fields must be done via public API functions and macros.
***********************************************************************/

typedef struct BagelValue {
    int type;
    union {
        bool boolean;
        double number;
        void* obj;
    } as;
} BagelValue;
typedef BagelValue (*BagelCFunc)(int argCount, BagelValue* args);

extern const BagelValue BAGEL_NIL;
extern const BagelValue BAGEL_TRUE;
extern const BagelValue BAGEL_FALSE;

BagelValue bagel_error(const char* message, ...);

typedef struct BagelMethod {
    const char* name;
    BagelCFunc method;
    int arity;
} BagelMethod;
BagelValue bagel_newclassx(const BagelMethod* methods, const char* name,
                           BagelValue inherit);
#define bagel_newclass(methods, name) bagel_newclassx(methods, name, BAGEL_NIL)
bool bagel_hasfield(BagelValue instance, const char* name);
BagelValue bagel_getfield(BagelValue instance, const char* name);
void bagel_setfield(BagelValue instance, const char* name,
                    BagelValue newvalue);
bool bagel_deletefield(BagelValue instance, const char* name);
BagelValue bagel_this(void);
BagelValue bagel_super(const char* field);
BagelValue bagel_call(BagelValue func, int nargs, BagelValue* args);
BagelValue bagel_invoke(BagelValue instance, const char* method, int nargs,
                        BagelValue* args);
BagelValue bagel_superinvoke(const char* method, int nargs, BagelValue* args);

BagelValue bagel_getglobal(const char* name);
void bagel_setglobal(const char* name, BagelValue newvalue);

typedef enum BagelType
{
    BAGEL_TNIL,
    BAGEL_TBOOL,
    BAGEL_TNUMBER,
    BAGEL_TERROR,
    BAGEL_TSTRING,
    BAGEL_TCLASS,
    BAGEL_TINSTANCE,
    BAGEL_TFUNC,
    BAGEL_TMETHOD,
    BAGEL_TDATA,
} BagelType;

BagelType bagel_type(BagelValue value);

bool bagel_tobool(BagelValue value);
double bagel_todoublex(BagelValue floatvalue, bool* success);
#define bagel_todouble(floatvalue) bagel_todoublex(floatvalue, NULL)
int64_t bagel_tointx(BagelValue value, bool* success);
#define bagel_toint(intvalue) bagel_tointx(intvalue, NULL)
const char* bagel_tostringn(BagelValue stringvalue, size_t* length);
#define bagel_tostring(stringvalue) bagel_tostringn(stringvalue, NULL)
void* bagel_todata(BagelValue value);
const char* bagel_datatype(BagelValue value);
void* bagel_checkdata(BagelValue value, const char* type);
#define bagel_getdata(data, type) ((type*)bagel_checkdata(data, #type))

BagelValue bagel_argerror(int arg, BagelType expected);

BagelValue bagel_bool(bool value);
BagelValue bagel_int(int64_t value);
BagelValue bagel_double(double value);
BagelValue bagel_string(const char* cstring);
BagelValue bagel_stringn(const char* cstring, size_t length);
BagelValue bagel_data(size_t size, const char* type);
#define bagel_newdata(type) bagel_data(sizeof(type), #type)
BagelValue bagel_cfunc(BagelCFunc func, int arity);
void bagel_setcfunc(const char* name, BagelCFunc func, int arity);

typedef enum BagelOp
{
    BAGEL_OPEQ,
    BAGEL_OPNE,
    BAGEL_OPLT,
    BAGEL_OPLE,
    BAGEL_OPGT,
    BAGEL_OPGE,
    BAGEL_OPADD,
    BAGEL_OPSUB,
    BAGEL_OPMUL,
    BAGEL_OPDIV,
    BAGEL_OPMOD,
    BAGEL_OPPOW,
    BAGEL_OPNEG,
} BagelOp;
BagelValue bagel_op(BagelValue left, BagelValue right, BagelOp op);
#define bagel_opbool(left, right, op) bagel_tobool(bagel_op(left, right, op))

// for memory safety when objects are referenced from C variables
// these use the stack, no need to clean up before return or raise
void bagel_push(BagelValue value);
BagelValue bagel_pop(int count);
// these are for long-term references that survive return/raise
// not until collections...
// uint32_t bagel_ref(BagelValue value);
// void bagel_dropref(uint32_t ref);

/* Standard Library */

void bagel_loadstdlib(void);

#ifdef __cplusplus
}
#endif
#endif // include guard
