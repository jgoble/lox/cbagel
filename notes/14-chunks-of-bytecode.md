# Chapter 14: Chunks of Bytecode

- [Link to book chapter](https://craftinginterpreters.com/chunks-of-bytecode.html)
- [Table of Contents](index.md)
- [Next chapter](15-a-virtual-machine.md)

## 14.1: Bytecode?

- Tree-walk interpreters:
  - Simple, portable implementation
  - ssssssllllooooooowwwwww!
  - Bad memory usage (both quantity and locality)
- Native machine code:
  - Fastest
  - Difficult and complex
  - Completely non-portable
- Bytecode:
  - Portable
  - Not as simple as tree-walking, but much easier than machine code
  - Slower than native code, but faster than tree-walking
  - Essentially machine code for a non-existent architecture
    - We build an *emulator* for that architecture -- a virtual machine

## 14.2: Getting Started

Nothing to note here, just setting up the basic framework.

## 14.3: Chunks of Instructions

- A *chunk* is a sequence of bytecode, or series of instructions
- The format used here uses a one-byte operation code, aka *opcode*,
  per instruction
- Bytecode is stored in a dynamic array
  - We allocate a new array and copy elements if we need a larger array
- `reallocate()` in `memory.c` handles all memory operations,
  including (re)allocating a dynamic array
  - This will be important for garbage collection later

## 14.4: Disassembling Chunks

- An assembler converts human-readable instruction names
  and converts them to the equivalent machine code
- A disassembler is the opposite; it converts machine code to text
- Disassemblers are useful for debugging bytecode
- The book's implementation allows instructions to have different sizes
  - Is that better or worse than all instructions having the same size?
  - Python switched from differently-sized instructions (one or three bytes)
      to same-sized instructions (all two bytes) in Python 3.6;
      obviously there was *some* benefit

## 14.5: Constants

- Chunks need to store data as well as code
  - An *immediate instruction* stores a small constant directly in the bytecode
  - Data can also be stored in a separate set of constants in the executable
  - clox puts all data in this *constant pool*, without immediate instructions
- Instructions can have operands -- binary data immediately after the opcode
  that serve as parameters to the instruction
- `OP_CONSTANT` uses an operand as an index into the constant pool

## 14.6: Line Information

- Bytecode needs to store line numbers for use in error messages
- Line numbers should not be interleaved with instructions
  because that causes more cache misses
- The book's version of clox uses a very simple means of storing this
  that is extremely memory inefficient
  - Fixing this is challenge 1 for this chapter (see issue #30)
- A single line of source code can become many instructions
