# Chapter 23: Jumping Back and Forth

- [Link to book chapter](https://craftinginterpreters.com/jumping-back-and-forth.html)
- [Previous chapter](22-local-variables.md)
- [Table of Contents](index.md)
- [Next chapter](24-calls-and-functions.md)

## Lede

- Control flow is the way execution moves through the program
- In the simplest case, we just increment the `ip` pointer, but some
  statements require skipping or repeating other statements
- We do this with *jumps* -- in the end, everything devolves to a `goto`

## 23.1: If Statements

- When we jump forward, we don't know how far forward to jump when the jump
  instruction is emitted. So we emit a placeholder operand and then *backpatch*
  it later once we know how much to skip.
- For clox, we leave the condition value on the stack when we decide whether to
  jump and emit separate POP opcodes later, so that the opcode can be reused for
  the `and` and `or` operators later. I would prefer an opcode that pops
  *and* jumps at once since if statements will probably use it far more than
  `and` and `or`.

## 23.2: Logical Operators

- Likewise, I think the minimalistic approach taken by the book is not a good way
  to do this. A separate JUMP_IF_TRUE opcode would be better for `or`.

## 23.3: While Statements

- Separate opcodes for forward and backward jumps avoids the need to encode a
  signed integer in the bytecode. I like this.

## 23.4: For Statements

- Creative use of jump instructions enables fully functional for loops
- I would probably rename `OP_LOOP` to `OP_JUMP_BACK`
- According to the book, clox is now Turing complete
