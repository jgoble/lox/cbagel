# Chapter 26: Garbage Collection

- [Link to book chapter](https://craftinginterpreters.com/garbage-collection.html)
- [Previous chapter](25-closures.md)
- [Table of Contents](index.md)
- [Next chapter](27-classes-and-instances.md)

## Lede

- Garbage collection is perfect for automation because it's necessary, tedious,
  and error-prone, and machines excel at it
- A garbage collector cannot determine what memory *will* be read in the future,
  only what memory *could* be read -- in other words, what memory is **reachable**
  from **roots**: any object the VM can reach directly without going through
  another object
- Reachability is defined inductively:
  - Roots are always reachable
  - Anything referenced by a reachable object is reachable
- This creates a recursive algorithm:
  - Start at the roots
  - Visit everything they reference, recursively
  - Free everything not visited

## 26.1 Mark-Sweep Garbage Collection

- The **mark-and-sweep** algorithm involves two phases:
  - Marking: Traverse all reachable objects starting at the roots and mark it
    in some manner
  - Sweeping: Free all unmarked objects
- Debugging a garbage collector is difficult because it is invisible, so debug
  logging and forcing frequent calls to the collector are helpful

## 26.2 Marking the Roots

- The main sources of roots are the VM stack and the hash table of global variables
- The closures in the stack of CallFrames are also roots
- Open upvalues are another set of roots
- The compiler also has roots, namely the functions it is compiling

## 26.3: Tracing Object References

- While tracing the graph, the GC uses three colors as an abstraction:
  - White: an object that hasn't been marked
  - Gray: an object that has been marked but hasn't been traced through,
    and may still refer to white objects
  - Black: an object that has been marked along with everything it directly
    refers to, and therefore does not point to any white objects
- In clox's implementation, a marked object is placed on a gray stack waiting
  to be blackened; once removed from that stack, it is implicitly black, so
  there is no actual encoding of black

## 26.4: Sweeping Unused Objects

- Sweeping is relatively simple: walk the list of objects, unmarking black objects
  and freeing white objects
- One issue is the string interning table: it is not a root, but refers to strings
  that may get freed; this is called a **weak reference**. Therefore after tracing
  is done, we walk the string table and delete any white string from the table.

## 26.5: When to Collect

- The question of when to run the GC is difficult to answer and relies on two key
  metrics: **throughput** and **latency**
  - Throughput is the percentage of time spent in user code, versus doing GC stuff;
    this affects the overall speed of a script, and higher is better
  - Latency is the longest single chunk of time where the user's program is stopped
    for GC work; this affects responsiveness or "snappiness" in a program, and
    lower is better
- It's a trade-off, and no single solution is the best for all scripts; most
  production languages provide a function or module in their standard library so that
  the user can tune the GC to their needs
- Our solution is to track the amount of memory allocated and do a collection when it
  goes above some threshold, then calculate the new allocation amount and set a nee
  threshold above that

## 26.6: Garbage Collection Bugs

- Garbage collection mistakes are easy to make and extremly difficult to debug; it's
  easy to accidentially free a live object (resulting in invalid memory access) or
  fail to free a dead object (resulting in a memory leak)
- This errors often don't instantly show up, making it difficult to find the source
  of the problem
- One of the most common sources of errors is an object stored only as a pointer on
  the C stack
