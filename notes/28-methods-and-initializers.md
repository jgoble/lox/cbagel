# Chapter 28: Methods and Initializers

- [Link to book chapter](https://craftinginterpreters.com/methods-and-initializers.html)
- [Previous chapter](27-classes-and-instances.md)
- [Table of Contents](index.md)
- [Next chapter](29-superclasses.md)

## 28.1: Method Declarations

- Methods are stored in a hash table on the class object
- The clox VM creates an empty class and then mutates it, one method at a time.
  This is notably different from Python, in which the class declaration is turned
  into an intermediate code object that is then called to execute the class body.
  I like Python's approach better.

## 28.2: Method References

- When a method is looked up, it needs to remember the instance on which it was
  called, even if it is stored in a variable and not called until later

## 28.3: This

- Methods access their receiver via the keyword `this`, which in many ways acts like
  a local variable that can be captured as an upvalue like any other local variable
- `this` is stored in stack slot zero, which previously held an unused reference to
  the function

## 28.4: Instance Initializers

- Lox treats a method named `init` as an initializer and runs it automatically
  during instance creation
- This initializer is prohibited from returning an explicit value, but instaead
  implicitly returns `this`

## 28.5: Optimized Invocations

- One of a bytecode interpreter's largest performance bottleneck is instruction
  decoding and dispatch
- When two instructions are frequently used together, fusing them into a
  single *superinstruction* that performs the actions of both can greatly speed
  up the VM
- Here, we have a situation where OP_GET_PROPERTY is usually followed by OP_CALL
  - This particular case is not just about instruction decoding; OP_GET_PROPERTY
    spends time allocating a new bound method object, initializing its fields,
    and then OP_CALL rips them right back out
  - In addition, all of these temporary bound method objects take time to be
    cleaned up when the garbage collector runs
