# Chapter 19: Strings

- [Link to book chapter](https://craftinginterpreters.com/strings.html)
- [Previous chapter](18-types-of-values.md)
- [Table of Contents](index.md)
- [Next chapter](20-hash-tables.md)

## Lede

- Current types are all small and stored within the `Value` struct
- Strings can be much larger and need to be dynamically allocated on the heap

## 19.1: Values and Objects

- The `Value` struct stores a pointer to the heap-allocated string
- Actually, a variety of objects will be stored this way, including instances and functions
- These objects have unique data but also shared state needed by the garbage collector

## 19.2: Struct Inheritance

- We use a single `Obj` struct with the shared state, and separate structs per type
  that have an `Obj` as their first element
- This inner `Obj` struct is expanded in place, and C guarantees that a pointer to
  the type struct is (properly converted) also a pointer to the first element
  (the `Obj` struct), allowing conversion back and forth via casting
- Macros that use an argument twice will evaluate that expression twice; this is bad if
  the expression has side effects, so use an `inline` function instead

## 19.3: Strings

- Every `ObjString` owns its own copy of its characters on the heap so that it can
  free them when no longer needed
- `allocateObject` and `allocateString` essentially serve as constructors for the
  "base" and "derived" classes, respectively

## 19.4: Operations on Strings

- String equality is more complicated because the entire string has to be walked,
  since two equal strings may have different pointers
- Concatenation requires the `OP_ADD` instruction to dynamically switch on the
  types of the operands since types are not known until runtime

## 19.5: Freeing Objects

- Dynamic allocation makes memory leaks easy
- Garbage collection is the full solution, but for now we should at least make sure
  all objects are accessible with a linked list of objects
