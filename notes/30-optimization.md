# Chapter 30: Optimization

- [Link to book chapter](https://craftinginterpreters.com/optimization.html)
- [Previous chapter](29-superclasses.md)
- [Table of Contents](index.md)

## 30.1: Measuring Performance

- Optimization can take many forms, including speed, memory usage, startup
  time, disk space, or bandwidth
- In order to optimize, one must first observe and measure to see where
  there are problems
- To do that, we write **benchmarks**: programs in our language that exercise
  or stress some specific part of the language, and are designed to measure
  how long something takes
- Complementing this is a **profiler**, a tool that can measure how much time
  the VM spends in each C function, among other metrics

## 30.2: Faster Hash Table Probing

- The book author's benchmarks and profiler discovered that clox was spending
  72% of its time on a particular benchmark performing hash table lookups,
  and 70% on a single line in `findEntry()`
  - Turns out the modulo operator is really, *really* slow!
- While we can't optimize a basic arithmetic operation, we do know more about
  our problem than the C compiler does
- We use modulo to wrap a hash code to fit in the table array, which starts at
  eight slots and grows by a factor of two every time. Hence, it is always a
  power of two, which makes bit masking accomplish the same thing that modulo
  does but much faster.
  - Performing modulo by a power of two is the same as bitwise AND with one
    less than that power of two
- Changing four modulos to bitwise ANDs doubled the speed of the author's
  benchmark
- It's not worth it to cache the result of the decrement; decrementing is so
  fast at the assembly level that modern instruction pipelining basically
  gives it to us for free if the CPU is slow somewhere else
- This is an example of an optimization that we wouldn't notice on our own;
  only a profiler can point us at it

## 30.3: NaN Boxing

- clox's `Value` type occupies 16 bytes on a 64-bit machine (4 bytes for the
  type tag, 8 for the largest union fields, and 4 for padding). Reducing this
  would make more `Value`s fit in a cache line, improving speed. However, we
  need both the actual value (which needs 8 bytes itself) *plus* information
  about its type.
- NaN boxing is a potential solution, especially for languages like Lox and
  earlier versions of Lua where there is no integer type, only `double`s
- NaN boxing exploits unused bits in the special NaN (Not a Number) value in
  the IEEE 754 floating-point standard
  - A double-precision floating point number reserves 1 bit for the sign,
    11 bits for the exponent, and 52 bits for the mantissa
  - NaN values are defined as any value where all exponent bits are set (1),
    regardless of the sign and mantissa bits
  - The highest mantissa bit is significant, denoting a "quiet" or "signaling"
    NaN, and the next highest is used by Intel for a special value
  - That leaves us with the lowest 50 bits and the highest bit (the sign bit),
    for a total of 51 bits with no meaning in a NaN
  - While pointers on a 64-bit system are technically 64 bits, it's difficult
    to find an architecture that uses anything other than the low 48 bits.
  - Hence, within 51 bits of a single 64-bit double, we have space for a
    pointer and a few type tags to distinguish between non-number values
- NaN boxing may not work on every architecture, so it needs to be guarded
  by preprocessor directives
- Since many operations here require bit operations, we use a 64-bit integer
  as the `Value` type, which requires type punning to interpret the bits as
  a `double`
- `nil` and Booleans are handled by claiming the bottom two bits of the
  mantissa as a type tag
- For `Obj` pointers, we use the sign bit to signal that it is a pointer
- The difficulty of this is that it is challenging to benchmark the performance
  improvement since NaN boxing is implemented entirely with macros that are
  expanded in place everywhere throughout the code
- In the end, while I implemented this for academic purposes, I reverted the
  commit because NaN boxing does not fit with my future vision for this
  language, especially as one of my long-term plans is to introduce an integer
  type, where every possible bit pattern could be a valid integer
