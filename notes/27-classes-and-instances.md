# Chapter 27: Classes and Instances

- [Link to book chapter](https://craftinginterpreters.com/classes-and-instances.html)
- [Previous chapter](26-garbage-collection.md)
- [Table of Contents](index.md)
- [Next chapter](28-methods-and-initializers.md)

## 27.1: Class Objects

- Classes are treated as objects just like strings and functions
- Simple implementation of empty class objects

## 27.2: Class Declarations

- Class declarations are also very simple to parse

## 27.3: Instances of Classes

- All Lox class instances are a single object type to the C code: "instance"
- Lox instances are created by simply calling the class like a function, in the
  same way as Python

## 27.4: Get and Set Expressions

- The dot for property access is essentially an infix operator
- Overall, this is a very simple and straightforward chapter
