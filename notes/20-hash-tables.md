# Chapter 20: Hash Tables

- [Link to book chapter](https://craftinginterpreters.com/strings.html)
- [Previous chapter](19-strings.md)
- [Table of Contents](index.md)
- [Next chapter](21-global-variables.md)

## Lede

- We need a way to store variables and look them up by name
- We also need a way to store fields on instances later
- Hash tables are the perfect data structure for this because they can
  look up a key in constant time

## 20.1: An Array of Buckets

- Imagine if Lox only permitted single lowercase letters as variables
  - We could just allocate an array of 26 pointers (or "buckets")
  - Access would be by subtracting `'a'` from the variable letter and
    using the result to index directly into the array of buckets
  - Simple, fast, and compact
  - ...but difficult to program in such a language
- Doing this with variables up to eight letters long would require nearly
  300,000 petabytes of memory!
  - Solution: an array large enough but not unreasonably large
  - Index by taking the key value modulo the array size
  - No wasted space!
  - ...but keys can collide.
- This is essentially a basic hash table
  - Hash tables track collision chances via the *load factor*, defined as
    (number of entries) / (number of buckets)
  - When the load factors gets too high, resize and grow the array

## 20.2: Collision Resolution

- Collisions can occur even with a low load factor, and can never be
  completely eliminated
- There are two broad categories of collision resolution. The first is
  **separate chaining**, and the second is confusingly called either
  **open addressing** or **closed hashing**.
- With separate chaining, each bucket contains a collection of items,
  often a linked list
  - This degrades to linear time lookup in the worst case when all entries
    are in the same bucket, but this is rare
  - Not a good fit for modern CPUs due to pointer overhead and poor
    data locality
- With open addressing/closed hashing, all entries are in the array directly,
  with no bucket containing more than one entry
  - This is compact and fast in memory, but makes all operations more complex
  - Collisions are resolved by finding a new bucket, the process of which
    is called *probing* and the order of which is a *probe sequence*
    - Multiple algorithms exist to calculate probe sequences, such as
      "double hashing", "cuckoo hashing", and "Robin Hood hashing"
    - This book keeps it simple and uses *linear probing*: if an entry is
      not in its proper bucket, we look in the next bucket, then the next one,
      and so on, wrapping around if needed
    - Linear probing is good for caching but is heavily prone to clustering

## 20.3: Hash Functions

- We need to be able to take a string key of *any* length and convert it to a
  fixed-size integer
- A *hash function* takes any blob of data and performs this conversion, with
  the resulting *hash code* depending on all bits of the original blob
- A good hash function must be:
  - Deterministic: the same hash code must always be produced for the same input
  - Uniform: given typical inputs, resulting hash codes should be distributed
    widely and evenly to reduce clustering
  - Fast: since every operation requires calculating the hash, a slow hash can
    erase the speed of the hash table
- This book uses a simple, well-used function known as
  [FNV-1a](http://www.isthe.com/chongo/tech/comp/fnv/)

## 20.4: Building a Hash Table

- Load factor is the ratio of `count` to `capacity`
- Allocating a string is already linear time like hashing, so it makes sense to
  hash a string once during allocation and then cache the result for future lookups
- A typical hash function starts with a constant with particular mathematical
  properties, then walks the data; for each byte or word, mix the bits into the
  hash value and then scramble the bits around
- This book uses an arbitrary maximum load factor of 75%
- When growing a hash table, we can't just let the C standard library copy
  everything over because entries may change buckets, so we have to rebuild the
  hash table from scratch
- Deleting a key can break a probe sequence, so we use a *tombstone* to mark
  such buckets so that keys further down the probe sequence can still be found

## 20.5: String Interning

- Comparing strings character-by-character is slow; what if we could just
  compare pointers?
- This requires that we cannot have two equal but distinct strings in existence
- String interning is a solution that involves creating a collection of unique
  strings; such that when you intern a string that is already in the collection,
  the original is used instead
- This speeds up string equality, which speeds up method name lookup later
