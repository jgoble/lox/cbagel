# CBagel: extending the Lox language in C

[![pipeline status](https://gitlab.com/jcgoble3/cbagel/badges/main/pipeline.svg)](https://gitlab.com/jcgoble3/cbagel/-/commits/main)

This started as an implementation of the Lox language described in Part III of the book
[*Crafting Interpreters*](https://craftinginterpreters.com/).
I added some of my own ideas along the way,
and now am building it into a full-featured scripting language with a new name: CBagel, or just Bagel.
(Technically, Bagel is the language, and CBagel is the reference implementation.
Kinda like Python and CPython.)

This project is being used as an independent study project (CS 4970) toward my computer science degree at Wright State University.
For notes that I compiled while working through the book, see [the notes folder](notes/index.md).

## Building

This implementation uses CMake as a build system. To configure, run the following commands:

```sh
mkdir build
cd build
cmake ..
cmake --build .
```

Alternatively, the last command can be substituted with `make` if your system has a compatible `make` utility.

## Additional features

A number of additional features have already been implemented;
check the closed issues and merged merge requests for details.
Planned future enhancements are tracked in open issues.
Work is performed in draft merge requests and then squashed onto the HEAD of the `main` branch.
